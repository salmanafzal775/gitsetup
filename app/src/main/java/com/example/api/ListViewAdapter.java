package com.example.api;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Hero> {
    private List<Hero> heroList;

    private Context mCtx;

    public ListViewAdapter(List<Hero> heroList, Context mCtx){
        super(mCtx, R.layout.list_items, heroList);
        this.heroList = heroList;
        this.mCtx = mCtx;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);

        View listViewItem = inflater.inflate(R.layout.list_items, null, true);

        TextView textViewName = listViewItem.findViewById(R.id.textViewName);
        ImageView textViewImageUrl = listViewItem.findViewById(R.id.textViewImageUrl);

        Hero hero = heroList.get(position);
        textViewName.setText(hero.getName());
        textViewImageUrl.setImageResource(hero.getImageUrl());
        Picasso.get().load("https:\\/\\/simplifiedcoding.net\\/demos\\/view-flipper\\/images\\/spiderman.jpg").into(textViewImageUrl);


        return listViewItem;

    }
}
